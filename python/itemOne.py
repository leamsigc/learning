class Node(object):
    def __init__(self, val):
        self.val = val
        self.next = None

    def get_data(self):
        return self.val

    def get_next(self):
        return self.next

    def set_next(self, next):
        self.next = next


class LinkedList(object):
    def __init__(self, head=None):
        self.head = head
        self.count = 0

    def get_count(self):
        return self.count

    def insert(self, data):
        # TODO : inset a new node
        new_node = Node(data)
        new_node.set_next(self.head)
        self.head = new_node
        self.count += 1

    def find(self, val):
        # TODO : find the first item with the given value
        item = self.head
        while (item != None):
            if item.get_data() == val:
                return item
            else:
                item = item.get_next()

        return None

    def delete_at(self, index):
        # TODO : delete and item at given index
        if index > self.count-1:
            return
        if index == 0:
            self.head = self.head.get_next()
        else:
            temIndex = 0
            temNode = self.head
            while temIndex < index - 1:
                temNode = temNode.get_next()
                temIndex += 1
            temNode.set_next(temNode.get_next().get_next())
            self.count -= 1

    def dump_list(self):
        temnode = self.head
        while(temnode != None):
            print('Node: ', temnode.get_data())
            temnode = temnode.get_next()


itemList = LinkedList()
itemList.insert(38)
itemList.insert(49)
itemList.insert(13)
itemList.insert(15)

itemList.dump_list()


print('Item count :', itemList.get_count())
print('Find item :', itemList.find(13))
print('Finding items :', itemList.find(78))


itemList.delete_at(3)
print('Item count:', itemList.get_count())
print('Finding items:', itemList.find(38))
itemList.dump_list()
